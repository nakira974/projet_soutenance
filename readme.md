#############################################################################################
#Auteur: Maxime Loukhal, Deletoille Antoine LPRGI PROJET SOUTENANCE 2020 CRYPTAGE DE DOSSIER #
#############################################################################################

#############################################################################################
#Section: LPRGI Initial PROJET SOUTENANCE 2020 CRYPTAGE DE DOSSIER                           #
#############################################################################################

#############################################################################################
#Projet: PROJET SOUTENANCE 2020 CRYPTAGE DE DOSSIER                                          #
#############################################################################################


---------------------------------
| Présentation de la solution : |
---------------------------------

La solution est une application console, avec un menu dans lequel il est possible de crypter un dossier à partir de son chemin et d'un mot de passe
demandé à l'utilisateur. Cela transforme le fichier en .aes, et il est également possible de le décrypter en utilisant le chemin de ce fichier,
le chemin d'un fichier sortie pour afficher le contenu, et le mot de passe prècdemment saisi.

Exemples: 

**CRYPTER**

> Saisir le chemin du fichier à crypter : 

/home/maxime/Documents/projet_git/projet_soutenance/Cryptage_Dossier/Files2_crypt/1MB.txt


> Saisir le mot de passe : 

1234

(Dans le même repertoire que le fichier, ce dernier est devenu 1MB.txt.aes)

**DECRYPTER**

> Saisir le chemin du fichier à decrypter : 

/home/maxime/Documents/projet_git/projet_soutenance/Cryptage_Dossier/Files2_crypt/1MB.txt.aes

>  Saisir le chemin du fichier où placer le contenu :

/home/maxime/Documents/projet_git/projet_soutenance/Cryptage_Dossier/Files2_crypt/decrypt.txt

> Saisir le mot de passe : 

1234

(Le contenu texte de 1MB.txt.aes est maintenant lisible dans decrypt.txt ! )






