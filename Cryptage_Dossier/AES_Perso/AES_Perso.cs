using System;
using System.IO;
using System.Security.Cryptography;
using System.Runtime.InteropServices;


namespace AES_Perso
{
    public class AES_Perso
    {
        
        
        //  Appelle cette fonction pour effacer la clé après utilisation pour plus de sécruité
        //D'où l'utilisation d'un pointeur.
        [DllImport("KERNEL32.DLL", EntryPoint = "RtlZeroMemory")]
        public static extern bool ZeroMemory(IntPtr Destination, int Length);
        
        
        /// <summary>
        /// Créer un salt qui va être utilisé pour crypter notre fichier. Cette méthode est requise dasn : FileEncrypt.
        /// </summary>
        /// <returns></returns>
        public static byte[] GenerateRandomSalt()
        {
            byte[] data = new byte[32];

            using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
            {
                for (int i = 0; i < 10; i++)
                {
                    // Rempli le buffer avec les data générés
                    rng.GetBytes(data);
                }
            }

            return data;
        }
        
             /// <summary>
/// Chiffre un fichier à partir de son chemin d'accès et d'un mot de passe simple.
/// </summary>
/// <param name="inputFile"></param>
/// <param name="password"></param>

             public static void FileEncrypt(string inputFile, string password)

             {
                 //http://stackoverflow.com/questions/27645527/aes-encryption-on-large-files
                 //On appelle notre méthode pour générer notre salt (donnée aléatoire)
                 byte[] salt = GenerateRandomSalt();
                 //créer un nom de fichier de sortie
                 FileStream fsCrypt = new FileStream(inputFile + ".aes", FileMode.Create);
                 //converti le mdp string en tableau de byte 
                 byte[] passwordBytes = System.Text.Encoding.UTF8.GetBytes(password);
                 //On applique l'algorithme de cryptage symétrique Rijndael 
                 RijndaelManaged AES = new RijndaelManaged();
                 AES.KeySize = 256;
                 AES.BlockSize = 128;
                 AES.Padding = PaddingMode.PKCS7;
                 //http://stackoverflow.com/questions/2659214/why-do-i-need-to-use-the-rfc2898derivebytes-class-in-net-instead-of-directly
                 //l'algo hache à plusieurs reprises le mot de passe utilisateur avec le salt généré plus haut." Une itération élevée est importante.
                 var key = new Rfc2898DeriveBytes(passwordBytes, salt, 50000);
                 AES.Key = key.GetBytes(AES.KeySize / 8);
                 AES.IV = key.GetBytes(AES.BlockSize / 8);
                 //Cipher modes: http://security.stackexchange.com/questions/52665/which-is-the-best-cipher-mode-and-padding-mode-for-aes-encryption
                 //CBC - Cipher Block Chianing. 
   
                 //Chaque bloc de texte en clair est xoré avec le bloc de texte chiffré précédent avant d'être transformé, 
                 //garantissant que des blocs de texte brut identiques ne donnent pas des blocs de texte chiffré identiques lorsqu'ils sont dans l'ordre.
                 AES.Mode = CipherMode.ECB;
                 // écrire le salt au début du fichier de sortie, pour en faire en sorte que ça toujours aléatoire
                 fsCrypt.Write(salt, 0, salt.Length);
                 CryptoStream cs = new CryptoStream(fsCrypt, AES.CreateEncryptor(), CryptoStreamMode.Write);
                 FileStream fsIn = new FileStream(inputFile, FileMode.Open);
                 //Créer un buffer (1mb) donc seul ce montant sera alloué dans la mémoire et non dans tout le fichier
                 byte[] buffer = new byte[1048576];
                 int read;
                 try
                 {
                     while ((read = fsIn.Read(buffer, 0, buffer.Length)) > 0)
                     {
                         cs.Write(buffer, 0, count: read);
                     }
                     // Close up
                     fsIn.Close();
                 }
                 catch (Exception ex)
                 {
                     Console.WriteLine("Error: " + ex.Message);
                 }
    
                 finally
                 {
                     cs.Close();
                     fsCrypt.Close();
                 }
             }
             
             
             /// <summary>
             /// Décrypte un fichier chiffré avec la méthode FileDecrypt via son chemin et le mot de passe simple.
             /// </summary>
             /// <param name="inputFile"></param>
             /// <param name="outputFile"></param>
             /// <param name="password"></param>
             public static void FileDecrypt(string inputFile, string outputFile, string password)
             {
                 byte[] passwordBytes = System.Text.Encoding.UTF8.GetBytes(password);
                 byte[] salt = new byte[32];

                 FileStream fsCrypt = new FileStream(inputFile, FileMode.Open);
                 fsCrypt.Read(salt, 0, salt.Length);

                 RijndaelManaged AES = new RijndaelManaged();
                 AES.KeySize = 256;
                 AES.BlockSize = 128;
                 var key = new Rfc2898DeriveBytes(passwordBytes, salt, 50000);
                 AES.Key = key.GetBytes(AES.KeySize / 8);
                 AES.IV = key.GetBytes(AES.BlockSize / 8);
                 AES.Padding = PaddingMode.PKCS7;
                 AES.Mode = CipherMode.ECB;
                 CryptoStream cs = new CryptoStream(fsCrypt, AES.CreateDecryptor(), CryptoStreamMode.Read);
                 FileStream fsOut = new FileStream(outputFile, FileMode.Create);
                 int read;
                 byte[] buffer = new byte[1048576];
                 try
                 {
                     while ((read = cs.Read(buffer, 0, buffer.Length)) > 0)
                     {
                         fsOut.Write(buffer, 0, read);
                     }
                 }
                 catch (CryptographicException ex_CryptographicException)
                 {
                     Console.WriteLine("CryptographicException error: " + ex_CryptographicException.Message);
                 }
                 catch (Exception ex)
                 {
                     Console.WriteLine("Erreur: " + ex.Message);
                 }
                 try
                 {
                     cs.Close();
                 }
                 catch (Exception ex)
                 {
                     Console.WriteLine("Erreur en fermant CryptoStream: " + ex.Message);
                 }
                 finally
                 {
                     fsOut.Close();
                     fsCrypt.Close();
                 }
             }
   
        
        

        

    }
}