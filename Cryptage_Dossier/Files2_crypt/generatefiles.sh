#!/bin/sh
# shellcheck disable=SC1018
echo -n "Generateur de fichier de taille voulue "
echo ""
echo ""
echo -n " '1' pour créer un fichier"
echo ""
echo -n " '2' pour quitter"
echo ""
echo ""
PS3='Entrez votre choix: '
options=("Créer un fichier"  "Quitter")
select opt in "${options[@]}"
do
    case $opt in
        "Créer un fichier")
            echo -n "Entrez le nom du fichier (sera créé dans le repertoire actuel) : "
            read nom
            echo -n "Entrez la taille du fichier voulue en octets  : "
            read taille
            touch $nom
            cat /dev/urandom | tr -dc [:graph:] | head -c $taille > $nom
            echo -n "Le fichier "+$nom, "de taille"+$taille, "a été créé "
            ;;
        "Quitter")
            break
            ;;
        *) echo "Option invalide $REPLY";;
    esac
done