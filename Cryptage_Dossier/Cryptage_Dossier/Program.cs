﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;
using AES_Perso;
using AES_Perso = AES_Perso.AES_Perso;


/*
 *##################################################################################################################################
 *Auteur : Maxime Loukhal, Antoine Deletoille                                                                                                          #
 * Nom du Projet : Cryptage de dossier par AES(Advance Standard Encryption)                                                        #
 * AES (Sources + Définition et fonctionnement):                                                                                   #
 * #################################################################################################################################
 * Sources :                                                                                                                       #
 * https://fr.wikipedia.org/wiki/Advanced_Encryption_Standard,                                                                     #    
 * https://www.aescrypt.com/                                                                                                       #
 * https://docs.microsoft.com/en-us/dotnet/api/system.security.cryptography.aes?view=netframework-4.8                              #
 * #################################################################################################################################
 *L'algorithme prend en entrée un bloc de 128 bits (16 octets),                                                                    #
 * la clé fait 128, 192 ou 256 bits.                                                                                               #
 * Les 16 octets en entrée sont permutés selon une table définie au préalable.                                                     #
 * Ces octets sont ensuite placés dans une matrice de 4x4 éléments et ses lignes subissent une rotation vers la droite.            #
 * L'incrément pour la rotation varie selon le numéro de la ligne.                                                                 #
 * Une transformation linéaire est ensuite appliquée sur la matrice,                                                               #
 * elle consiste en la multiplication binaire de chaque élément de la matrice avec des polynômes issus d'une matrice auxiliaire    #
 *                                                                                                                                 #
 *                                                                                                                                 #
 *##################################################################################################################################
 */

namespace Cryptage_Dossier
{
    class Program
    {
        private static void MainMenu()
        {
            Stopwatch sw = new Stopwatch();
            string entree;
            string sortie;
            string mdp;
            Console.Clear();
            Console.WriteLine("Projet soutenance 2020 LPRGI, M.LOUKHAL, A.DELETOILLE");
            Console.WriteLine("©Association CompuCraft");
            Console.WriteLine("1) Crypter un fichier");
            Console.WriteLine("2) Decrypter un ficher");
            Console.WriteLine("3) Quitter");
            Console.Write("\r\nchoisir une option: ");
            
            switch (Console.ReadLine())
            {
                
                
                case "1":
                    Console.WriteLine("Saisir le chemin du fichier à crypter: ");
                    entree=Console.ReadLine();
                    Console.WriteLine("Saisir le mot de passe : ");
                    mdp = Console.ReadLine();
                    //GCHandle gchCrypt = GCHandle.Alloc(mdp, GCHandleType.Pinned);
                    try
                    {
                        sw.Start();
                        global::AES_Perso.AES_Perso.FileEncrypt(entree,mdp);
                        sw.Stop();
                        Console.WriteLine("Durée d'exécution: {0}", sw.Elapsed.TotalSeconds);
                        Thread.Sleep(5000);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        MainMenu();
                    }
                    
                    // Supprime le mdp de la mémoire 
                    //global::AES_Perso.AES_Perso.ZeroMemory(gchCrypt.AddrOfPinnedObject(), mdp.Length * 2);
                    //gchCrypt.Free();
                    //On vérifie que le mdp a bien disparu
                    //Console.WriteLine("Le mot de passe utilisé a été supprimé: " + mdp);
                    MainMenu();
                    break;
                    
                case "2":
                    Console.WriteLine("Saisir le chemin du fichier à Decrypter: ");
                    entree=Console.ReadLine();
                    Console.WriteLine("Saisir le chemin du fichier où placer le contenu : ");
                    sortie=Console.ReadLine();
                    Console.WriteLine("Saisir le mot de passe : ");
                    mdp = Console.ReadLine();
                    // Pour plus de sécurité Pin le mdp du fichier
                    //GCHandle gchDecrypt = GCHandle.Alloc(mdp, GCHandleType.Pinned);
                    try
                    {
                        sw.Start();
                        global::AES_Perso.AES_Perso.FileDecrypt(entree,sortie,mdp);
                        sw.Stop();
                        Console.WriteLine("Durée d'exécution: {0}", sw.Elapsed.TotalSeconds);
                        Thread.Sleep(5000);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        MainMenu();
                    }
                    
                    // Supprime le mdp de la mémoire 
                    //global::AES_Perso.AES_Perso.ZeroMemory(gchDecrypt.AddrOfPinnedObject(), mdp.Length * 2);
                    
                    //gchDecrypt.Free();
                    //Console.WriteLine("Le mot de passe utilisé a été supprimé: " + mdp);
                    MainMenu();
                    break;
                case "3":
                    Environment.Exit(0);
                    break;
            }
        }
        static void Main(string[] args)
        {
            MainMenu();
        }
    }
}
